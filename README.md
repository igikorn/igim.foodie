== OFFICIAL README ==
# Intro

## Spring hangs
spring stop

## Ember

## Rails 5 API
### CORS
Cors ~/config/intializers/cors.rb has been enabled to avoid issues when API is called from the frontend app. It is currently set to wildcard for local development.

### Gems used

#### active_model_serializers
ActiveModelSerializers brings convention over configuration to your JSON generation.

#### devise
Devise is a flexible authentication solution for Rails based on Warden.

#### omniauth
OmniAuth is a flexible authentication system utilizing Rack middleware.

#### devise_token_auth
Token based authentication for Rails JSON APIs.

### Rails Console
User.create({email: 'test@test.com', password: 'password'})
Post.create({title: "the first test", content: 'this is the content. amazing', user_id: '1'})

## Ember
ember install ember-cli-coffeescript
ember install ember-cli-sass
bower install bulma

Note - when using ember to install add-ons, they get saved to the package.json file. To uninstall:

bower uninstall package --save-dev

### Configure ember cli sass to use sass
```// ember-cli-build.js
  var app = new EmberApp(defaults, {
    sassOptions: {
      extension: 'sass'
    }
  });
```
Rename app/styles/app.css to app/styles/app.sass

## Other Notes
The project uses the jsonapi specification. http://jsonapi.org/

== END OFFICIAL README ==

rails g scaffold company name:string phone:string email:string website:string address:text customer_id:string additional_info:text

rails g scaffold contact family_name:string given_names:string company:reference title:string phone:string email:string website:string address:text customer_id:string

rails g scaffold post title:string content:string user:references

rails g scaffold review title:string content:string

rails g scaffold recipe title:string content:string

== NOTES ==
brew install watchman

https://emberigniter.com/uninstall-remove-ember-add-on/

https://www.netguru.co/tips/how-to-use-devise-authentication-with-emberjs

rails g scaffold company name:string phone:string email:string website:string address:text customer_id:string additional_info:text

rails g scaffold contact family_name:string given_names:string company:reference title:string phone:string website:string address:text customer_id:string additional_info:text

rails g scaffold project name:string status:integer

rails g scaffold tasks description:string status:integer project:references due_at:datetime


scaffold and set up relationships. Important - do it after active_model_serializer is installed.
migrate
rails g resource post title:string body:string

add constraints to models
- store common behaviour models should have
- validation
  validates __, presence: true


rails generate devise:install
rails generate devise User
rails db:migrate

rails g scaffold post title:string content:text


initializers
cors.rb - takes care of cross origin requests. allows browser to send optional requests to determine to check if origin is allowed to access a specific resource, with a specific http method.

config/initializers/cors.rb

active_model_serializers - allows control of json output of the api. rails only applies two chase method.

ember uses json api. standardized json response formatting.

ember new foodie-api -dir foodie
ember install ember-cli-coffeescript
ember install ember-cli-sass
bower install bulma

ember install ember-simple-auth
ember install ember-cli-ic-ajax


https://www.adibsaad.com/blog/token-authentication-with-ember-2-1-ember-simple-auth-1-0-0-and-devise_token_auth-0-1-36



https://emberigniter.com/modern-bridge-ember-and-rails-5-with-json-api/
rails new bookstore-api --api --database=postgresql


rails generate scaffold book title 'price:decimal{5,2}' author:references publisher:references{polymorphic}
rails g scaffold author name
rails g scaffold publishing_house name 'discount:decimal{2,2}'
rails db:migrate

Build a ‘Foodie’ Blogging platform

Users should be able to create, edit and delete three different object types:
1. Blog Post
2. Recipe
3. Review

Each action on an object should populate a global activity stream. This activity stream should display the most recent 20 items in the sidebar or the app.

There should be an admin page where User accounts can be added or removed.

We would prefer the app to be a single page app with a Rails api backend (ideally with an Ember front end).

Tips
Feel free to use Devise to quickly get users up and running
Feel free to use any UI framework of your choice to help get the layouts done quickly
Please do the activity feed itself from scratch. An activity item should include information about who did it, when they did it, what they did and a link to the item where appropriate
Bonus points for tests

Please review the instructions and the test and come back with an estimate on how many hours actual work you will need to complete and when you indent to deliver the result.

Let us know if you have any questions.

// fix brew
brew doctor
brew update

// install xcode
xcode-select --install

// use stable ruby
rvm install 2.3.1
rvm use 2.3.1

// install gem reqs
gem install bundler
gem install nokogiri

// install rails
gem install rails --pre

// install postgres
brew install postgresql
gem install pg
pg_ctl -D /usr/local/var/postgres -l logfile start
pg_ctl -D /usr/local/var/postgres -l logfile stop

// setup user and db
createuser -s postgres
psql -U postgres
create user foodie with password 'f00die';
# Create the databases
create database foodie owner foodie;
grant all privileges on database foodie to foodie;
# Allow user to create databases
ALTER USER foodie CREATEDB;

// postgresql db troubleshooting
rm -rf /usr/local/var/postgres
initdb /usr/local/var/postgres -E utf8

# rake setup
rake db:drop  # ERASES THE DATABASE !!!!
rake db:create
rake db:migrate
rake db:seed

// ember time...
npm install -g ember-cli
cd ~/emberdir
npm install
bower install



// start the servers
User.create({email: 'test@test.com', password: 'password'})
rails server --binding 0.0.0.0

// create a new application
rails new .

// Quick test - see if things run
rake -T

// run the server
rails server

// sassings...
find . -iname "*.less" -exec less2sass {} \;
sass-convert -R . --from scss --to sass


https://www.digitalocean.com/community/tutorials/how-to-setup-ruby-on-rails-with-postgres


http://www.tunnelsup.com/setting-up-postgres-on-mac-osx
# Create the rails project
rails new projectname --database=postgresql
cd projectname


# Create some models/controllers etc
rails generate controller site
rails generate model
# Run migrations
rake db:migrate
# Start the server
rails server

BDD Dev
https://semaphoreci.com/community/tutorials/setting-up-the-bdd-stack-on-a-new-rails-4-application


//check routes
rake routes

// devise testing
https://launchschool.com/blog/how-to-use-devise-in-rails-for-authentication
https://dockyard.com/blog/2014/05/07/building-an-ember-app-with-rails-part-1

// create new ember
ember new foodieember --skip-git

// rails c
Monologue::User.create(name: "igikorn", email:"igikorn@gmail.com", password:"monologue", password_confirmation: "monologue")


posts
  title:string
  content:text
  status:boolean
  user
  category:references

post_category

review
  content
  blog
  user

recipe
  title:string
  content:text
  status:boolean
  user
  category

generate scaffold post title content:text status:boolean user:references publisher:references{polymorphic}
