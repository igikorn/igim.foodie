`import DS from 'ember-data'`
`import ENV from 'foodie/config/environment'`
`import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin'`

ApplicationAdapter = DS.JSONAPIAdapter.extend DataAdapterMixin,
  authorizer: 'authorizer:devise'

`export default ApplicationAdapter`
