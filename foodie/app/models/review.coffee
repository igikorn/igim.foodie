`import Trackable from './trackable'`

Post = Trackable.extend
  title: DS.attr()
  content: DS.attr()
  excerpt: DS.attr()
  starRating: DS.attr()
  user: DS.belongsTo 'user'
  createdAt: DS.attr 'date'

  contentHtmlSafe: (->
    return "" unless @get('content')
    new Ember.Handlebars.SafeString @get('content').replace /\n/g, '<br>'
  ).property('content')

`export default Post`
