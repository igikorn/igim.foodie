`import Trackable from './trackable'`

Post = Trackable.extend
  title: DS.attr 'string'
  content: DS.attr 'string'
  excerpt: DS.attr 'string'
  user: DS.belongsTo 'user'
  createdAt: DS.attr 'date'

  contentHtmlSafe: (->
    return "" unless @get('content')
    new Ember.Handlebars.SafeString @get('content').replace /\n/g, '<br>'
  ).property('content')

`export default Post`
