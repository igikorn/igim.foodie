`import DS from 'ember-data'`
`import Trackable from 'foodie/models/trackable'`

User = Trackable.extend
  email: DS.attr(),
  name: DS.attr(),
  posts: DS.hasMany 'post'

`export default User`
