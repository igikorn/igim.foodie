`import DS from 'ember-data'`

Trackable = DS.Model.extend
  type: DS.attr()

`export default Trackable`
