`import DS from 'ember-data'`

Activity = DS.Model.extend
  action: DS.attr 'string'
  trackableType: DS.attr 'string'
  createdAt: DS.attr 'date'

  user: DS.belongsTo 'user'
  trackable: DS.belongsTo 'trackable', polymorphic: true, async: true

  formattedAction: (->
    if !@get('action')
        return ''
      actionPhrase = ''
      switch @get('action')
        when 'create'
          actionPhrase += 'created a'
        when 'update'
          actionPhrase += 'created a'
      actionPhrase
    ).property('action')

`export default Activity`
