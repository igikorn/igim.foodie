`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin'`

RecipesRoute = Ember.Route.extend(AuthenticatedRouteMixin)

`export default RecipesRoute`
