import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

const { service } = Ember.inject;

export default Ember.Route.extend(ApplicationRouteMixin, {
  poll: Ember.inject.service(),
  model: function () {
    return this.store.findAll('activity')
    // return Ember.RSVP.hash({
    //   activities: (this.get('session.isAuthenticated') ? this.store.findAll('activity') : [])
    // });
  },
  setupController: function (controller, model) {
    this._super(controller, model);
  },
  sessionAccount: service('session-account'),
  beforeModel() {
    return this._loadCurrentUser();
  },
  sessionAuthenticated() {
    this._loadCurrentUser().then(()=>{
      this.transitionTo('/');
    }).catch(() => this.get('session').invalidate());
  },
  _loadCurrentUser() {
    return this.get('sessionAccount').loadCurrentUser();
  },
  afterModel: function () {
    this._super(...arguments);
    this.get('poll').start({
      idle_timeout: 10000,
      interval: 3000
    });
  }
});
