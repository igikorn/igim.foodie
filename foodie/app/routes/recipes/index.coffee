`import Ember from 'ember'`

RecipesIndexRoute = Ember.Route.extend
  model: -> @store.findAll('recipe')

`export default RecipesIndexRoute`
