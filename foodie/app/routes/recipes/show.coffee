`import Ember from 'ember'`

RecipesShowRoute = Ember.Route.extend
  model: (params) -> @store.findRecord('recipe', params.recipe_id)

`export default RecipesShowRoute`
