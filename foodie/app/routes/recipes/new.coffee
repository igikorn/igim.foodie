`import Ember from 'ember'`

RecipesNewRoute = Ember.Route.extend
  model: ->
    Ember.RSVP.hash
      recipe: @store.createRecord('recipe')

  setupController: (controller, model) ->
    @_super(controller, model)
    controller.set('model', model.recipe)

  renderTemplate: -> @render('recipes/form')

  deactivate: -> @get('controller.model').rollbackAttributes()

`export default RecipesNewRoute`
