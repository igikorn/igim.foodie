`import Ember from 'ember'`

RecipesShowRoute = Ember.Route.extend
  model: (params) ->
    Ember.RSVP.hash
      recipe: @store.findRecord('recipe', params.recipe_id)

  setupController: (controller, model) ->
    @_super(controller, model)
    controller.set('model', model.recipe)

  renderTemplate: -> @render('recipes/form')

  deactivate: -> @get('controller.model').rollbackAttributes()

`export default RecipesShowRoute`
