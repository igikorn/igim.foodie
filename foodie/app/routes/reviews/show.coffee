`import Ember from 'ember'`

ReviewsShowRoute = Ember.Route.extend
  model: (params) -> @store.findRecord('review', params.review_id)

`export default ReviewsShowRoute`
