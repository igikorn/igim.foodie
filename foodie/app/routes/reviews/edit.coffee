`import Ember from 'ember'`

ReviewsShowRoute = Ember.Route.extend
  model: (params) ->
    Ember.RSVP.hash
      review: @store.findRecord('review', params.review_id)
      # companies: @store.findAll('company')

  setupController: (controller, model) ->
    @_super(controller, model)
    controller.set('model', model.review)
    # controller.set('companies', model.companies)

  renderTemplate: -> @render('reviews/form')

  deactivate: -> @get('controller.model').rollbackAttributes()

`export default ReviewsShowRoute`
