`import Ember from 'ember'`

ReviewsIndexRoute = Ember.Route.extend
  model: -> @store.findAll('review')

`export default ReviewsIndexRoute`
