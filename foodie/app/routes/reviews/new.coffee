`import Ember from 'ember'`

ReviewsNewRoute = Ember.Route.extend
  model: ->
    Ember.RSVP.hash
      review: @store.createRecord('review')

  setupController: (controller, model) ->
    @_super(controller, model)
    controller.set('model', model.review)

  renderTemplate: -> @render('reviews/form')

  deactivate: -> @get('controller.model').rollbackAttributes()

`export default ReviewsNewRoute`
