`import Ember from 'ember'`

PostsNewRoute = Ember.Route.extend
  model: ->
    Ember.RSVP.hash
      post: @store.createRecord('post')

  setupController: (controller, model) ->
    @_super(controller, model)
    controller.set('model', model.post)

  renderTemplate: -> @render('posts/form')

  deactivate: -> @get('controller.model').rollbackAttributes()

`export default PostsNewRoute`
