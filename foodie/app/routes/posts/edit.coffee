`import Ember from 'ember'`

PostsShowRoute = Ember.Route.extend
  model: (params) ->
    Ember.RSVP.hash
      post: @store.findRecord('post', params.post_id)
      # companies: @store.findAll('company')

  setupController: (controller, model) ->
    @_super(controller, model)
    controller.set('model', model.post)
    # controller.set('companies', model.companies)

  renderTemplate: -> @render('posts/form')

  deactivate: -> @get('controller.model').rollbackAttributes()

`export default PostsShowRoute`
