`import Ember from 'ember'`

PostsShowRoute = Ember.Route.extend
  model: (params) -> @store.findRecord('post', params.post_id)

`export default PostsShowRoute`
