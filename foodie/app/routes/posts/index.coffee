`import Ember from 'ember'`

PostsIndexRoute = Ember.Route.extend
  model: -> @store.findAll('post')

`export default PostsIndexRoute`
