`import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin'`
`import Ember from 'ember'`
`import ENV from 'foodie/config/environment'`

ActivitiesRoute = Ember.Route.extend AuthenticatedRouteMixin,
  poll: Ember.inject.service()
  model: (params) -> @store.findAll('activity')

  afterModel: (model, transition) ->
    @get('poll').setup
      name: 'activitiesPoll'
      resource_name: 'activities'
      url: ENV.apiHost + 'activities/'
  actions:
    willTransition: (transition) ->
      @_super transition
      @get('poll').removePoll 'activitiesPoll'

`export default ActivitiesRoute`
