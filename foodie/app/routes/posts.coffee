`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin'`

PostsRoute = Ember.Route.extend(AuthenticatedRouteMixin)

`export default PostsRoute`
