`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin'`

ReviewsRoute = Ember.Route.extend(AuthenticatedRouteMixin)

`export default ReviewsRoute`
