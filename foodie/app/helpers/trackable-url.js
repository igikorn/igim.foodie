import Ember from 'ember';
import ENV from 'foodie/config/environment'

export function trackableURL(params) {
  var inflector = new Ember.Inflector(Ember.Inflector.defaultRules);
  var postType = inflector.pluralize(params[0]);
  return '/' + postType.toLowerCase() + '/' + params[1];
}

export default Ember.Helper.helper(trackableURL);
