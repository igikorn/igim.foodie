import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('posts', function () {
    this.route('index', { path: '/' });
    this.route('show', { path: ':post_id' });
    this.route('edit', { path: ':post_id/edit' });
    this.route('new');
  });
  this.route('reviews', function () {
    this.route('index', { path: '/' });
    this.route('show', { path: ':review_id' });
    this.route('edit', { path: ':review_id/edit' });
    this.route('new');
  });
  this.route('recipes', function () {
    this.route('index', { path: '/' });
    this.route('show', { path: ':recipe_id' });
    this.route('edit', { path: ':recipe_id/edit' });
    this.route('new');
  });
  this.route('login');
  this.route('activities');
});

export default Router;
