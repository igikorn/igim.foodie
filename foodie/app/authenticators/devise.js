// app/authenticators/devise.js
import Devise from 'ember-simple-auth/authenticators/devise';
import ENV from 'foodie/config/environment'

export default Devise.extend({
  serverTokenEndpoint: ENV.apiHost + 'users/sign_in'
});
