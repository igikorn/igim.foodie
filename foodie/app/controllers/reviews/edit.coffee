`import Ember from 'ember'`

ReviewsEditController = Ember.Controller.extend
  actions:
    save: ->
      @get('model').save().then =>
        @transitionToRoute 'reviews.show', @get('model')

    chooseRating: (starRating) ->
      this.set('destination', starRating.value)

    cancel: -> @transitionToRoute 'reviews.show', @get('model')

`export default ReviewsEditController`
