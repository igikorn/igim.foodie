`import Ember from 'ember'`

ReviewsNewController = Ember.Controller.extend
  ratings: [1,2,3,4,5]
  actions:
    save: ->
      @get('model').save().then =>
        @transitionToRoute 'reviews.show', @get('model')

    cancel: -> @transitionToRoute 'reviews.index'

`export default ReviewsNewController`
