`import Ember from 'ember'`

ReviewsShowController = Ember.Controller.extend
  actions:
    delete: (model) ->
      model.destroyRecord().then =>
        @transitionToRoute 'reviews.index'

`export default ReviewsShowController`
