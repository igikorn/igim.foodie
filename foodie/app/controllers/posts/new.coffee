`import Ember from 'ember'`

PostsNewController = Ember.Controller.extend
  actions:
    save: ->
      @get('model').save().then =>
        @transitionToRoute 'posts.show', @get('model')

    cancel: -> @transitionToRoute 'posts.index'

`export default PostsNewController`
