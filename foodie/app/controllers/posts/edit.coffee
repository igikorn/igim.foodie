`import Ember from 'ember'`

PostsEditController = Ember.Controller.extend
  actions:
    save: ->
      @get('model').save().then =>
        @transitionToRoute 'posts.show', @get('model')

    cancel: -> @transitionToRoute 'posts.show', @get('model')

`export default PostsEditController`
