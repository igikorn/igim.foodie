`import Ember from 'ember'`

PostsShowController = Ember.Controller.extend
  actions:
    delete: (model) ->
      model.destroyRecord().then =>
        @transitionToRoute 'posts.index'

`export default PostsShowController`
