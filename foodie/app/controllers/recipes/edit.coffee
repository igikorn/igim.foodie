`import Ember from 'ember'`

RecipesEditController = Ember.Controller.extend
  actions:
    save: ->
      @get('model').save().then =>
        @transitionToRoute 'recipes.show', @get('model')

    cancel: -> @transitionToRoute 'recipes.show', @get('model')

`export default RecipesEditController`
