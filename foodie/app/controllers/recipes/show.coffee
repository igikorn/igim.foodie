`import Ember from 'ember'`

RecipesShowController = Ember.Controller.extend
  actions:
    delete: (model) ->
      model.destroyRecord().then =>
        @transitionToRoute 'recipes.index'

`export default RecipesShowController`
