`import Ember from 'ember'`

RecipesNewController = Ember.Controller.extend
  actions:
    save: ->
      @get('model').save().then =>
        @transitionToRoute 'recipes.show', @get('model')

    cancel: -> @transitionToRoute 'recipes.index'

`export default RecipesNewController`
