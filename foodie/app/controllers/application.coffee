`import Ember from 'ember'`

ApplicationController = Ember.Controller.extend
  session: Ember.inject.service()
  sessionAccount: Ember.inject.service('session-account')

  actions:
    logout: ->
      @get('session').invalidate()

`export default ApplicationController`
