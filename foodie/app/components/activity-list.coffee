`import Ember from 'ember'`
`import ENV from 'foodie/config/environment'`

ActivityListComponent = Ember.Component.extend
  store: Ember.inject.service()
  poll: Ember.inject.service()
  didInsertElement: ->
    @_super()
    query_params =
      some_param: 'some_value'
      other_param: 'other_value'
    @get('poll').setup
      name: 'activitiesPoll'
      resource_name: 'activity'
      url: ENV.apiHost + 'activities/'
      params: query_params
  willDestroy: ->
    @_super()
    @get('poll').removePoll 'activitiesPoll'
    # remove resource from polling
  sortedActivities: Ember.computed.sort('activities', 'sortDefinition')
  sortDefinition: ['date']

`export default ActivityListComponent`
