class ApplicationController < ActionController::API

  include ActionController::HttpAuthentication::Token::ControllerMethods
  before_action :authenticate_user_from_token!
  before_action :normalize_params, only: [:create, :update]

  # using the token authenticated user if available
  # before_filter :authenticate_user!

  def track_activity(trackable, action = params[:action])
    current_user.activities.create! action: action, trackable: trackable
  end

  private

  def normalize_params
    return unless params[:data] && params[:data][:attributes]

    params[:data][:attributes].keys.each do |key|
      unless key.to_s.underscore == key
        params[:data][:attributes][key.to_s.underscore] = params[:data][:attributes][key]
        params[:data][:attributes].delete(key)
      end
    end
  end

  def find_related_object(data)
    return unless data && data[:type] && data[:id]
    klass = data[:type].underscore.classify.safe_constantize
    klass.find(data[:id]) if klass
  end

  def authenticate_user_from_token!
    authenticate_with_http_token do |token, options|
      user_email = options[:email].presence
      user = user_email && User.find_by_email(user_email)

      if user && Devise.secure_compare(user.authentication_token, token)
        sign_in user, store: false
      else
        sign_out user
      end
    end
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      User.find_by(authentication_token: token)
    end
  end

  def current_user
    authenticate_token
  end
end
