class ActivitySerializer < ActiveModel::Serializer
  attributes :id, :action, :trackable_type, :created_at
  has_one :user
  belongs_to :trackable
end
