class ReviewSerializer < ActiveModel::Serializer
  attributes :id, :title, :content, :excerpt, :star_rating, :created_at
  has_one :user
end
