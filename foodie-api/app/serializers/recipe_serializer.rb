class RecipeSerializer < ActiveModel::Serializer
  attributes :id, :title, :content, :excerpt, :created_at
  has_one :user
end
