# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(
  email: 'dexter@lab.com',
  name: 'Dexter Boy Genius',
  password: 'password',
  password_confirmation: 'password'
)

user2 = User.create(
  email: 'johnny_bravo@hulks.com',
  name: 'Johnny Bravo',
  password: 'heresjohnny',
  password_confirmation: 'heresjohnny'
)

Post.create([
{
  title: 'Jimmy the Cook.',
  content: '<p><img src="http://i2.wp.com/www.eatherenow.co.nz/wp-content/uploads/2016/08/DST-GLRSC-3340.jpg?resize=630%2C473" alt="DST-GLRSC-3340"></p><br><p>We weren\'t very happy when Jimmy "the Fish" Gerard shut up shop in Ponsonby late last year: his Lion Red beer batter and gruff demeanour were the perfect antidote to Ponsonby Central\'s hipster surrounds. So we rejoiced when he popped up in the back corner of the Grey Lynn Returned Services Club. There are families with kids, guys playing pool, men in high-vis.</p><br><p>He still does seafood, thank goodness: tarakihi when it\'s fresh, pan-fried with caper butter and served with chips and a really good iceberg lettuce salad ($18) or in his aforementioned Lion Red beer batter ($15 adults, $10 kids). His fish pie, which is his mum\'s recipe (the secret ingredient is - yes - tinned asparagus) you can dine in on for a tenner, or take home to heat yourself. Recently - and this isn\'t said lightly - we ate just about the best gnocchi we\'ve ever tasted: pan fried with a crisp edge but all softly-softly on the inside, with a browned sage butter and punctuated with salty, perfect prosciutto. It was $10. We also loved his polenta chips with mayo and salsa ($5) and his crayfish bisque ($5) - "It\'s a soup," says the blackboard menu.</p><br><p>It\'s a rotating menu of whatever is good and available. Sometimes for pudding he\'ll put on creme caramel; the other day we had beef cheek ragu with mash and broccoli ($18), and it was perfectly, meltingly, falling apart and rich, on mash that tasted homemade. One punter was taking his ragout home in plastic containers: when he asked for his broccoli, Jimmy handed him half a head raw. He was busy. DM</p><br><p>HOURS Dinner, Wednesday to Saturday.</p><br><p>ADDRESS Grey Lynn RSC, 1 Francis Street, Grey Lynn.</p><br><p>IMPORTANT DETAILS Cash only - though the bar will let you withdraw some. Also: it\'s just Jimmy cooking, sometimes with a helper, so it can take a while.</p>',
  excerpt: 'We weren\'t very happy when Jimmy "the Fish" Gerard shut up shop in Ponsonby late last year...',
  user: user
},
{
  title: 'Let\'s eat here!',
  content: '<p><img src="http://i0.wp.com/www.eatherenow.co.nz/wp-content/uploads/2013/10/DST-LUCKYTACO-3651.jpg?resize=630%2C473"></p><p>Eat Here Now is the only online restaurant reviewing site that commissions a professional photographer to shoot every restaurant, whether that\'s a back-alley noodle bar or a fine diner. As a result, EHN and resident photographer David Straight have built up a comprehensive library of photographs of Auckland restaurants. We also have the beginnings of a library for our Wellington reviews.</p><p>If you are a blogger and would like to use one of these photos, please email us on hello@eatherenow.co.nz explaining which image you would like to use, what the purpose is and whether your blog carries any advertising – we love supporting independent bloggers and usually allow photos to be used as long as they are appropriately credited and include a link back to this site.</p><p>We also welcome enquiries for commercial use of these photographs whether that be from other websites, restaurants, agencies or print media though we are careful where our images wind up. We charge a yearly, renewable licence fee for this – prices depend on the situation and the number of photographs you want to use. We have both low-res and high-res versions available for online and print use, as well as a significant number of “out-takes” that didn\'t make it onto the site. Please email hello@eatherenow.co.nz and tell us what you want and how you want to use it and we\'ll see how we can help.</p><p>Please note this only relates to images that have been used on Eat Here Now. If you would like to commission David separately, his website is www.davidstraight.net and we can vouch for his both his professionalism and keen eye. He is a joy to work with and can be reached on ds (a) eatherenow.co.nz</p>',
  excerpt: 'Eat Here Now is the only online restaurant reviewing site that commissions a professional photographer to shoot every restaurant...',
  user: user2
}
])
