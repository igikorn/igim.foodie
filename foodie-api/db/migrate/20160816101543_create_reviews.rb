class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.string :title
      t.text :content
      t.text :excerpt
      t.references :user, foreign_key: true
      t.integer :star_rating, default: 0

      t.timestamps
    end
  end
end
