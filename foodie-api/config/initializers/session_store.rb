# The Rails application should also not issue session cookies but authentication
# should be done exclusively via the authentication token as described above.
# The easiest way to disable sessions in Rails is to add an initializer
# config/initializers/session_store.rb and disable the session store in that:

Rails.application.config.session_store :disabled
