Rails.application.routes.draw do
  resources :recipes
  resources :reviews
  devise_for :users, controllers: { sessions: 'users/sessions' }
  get '/users/me', to: 'users#me'
  resources :posts
  resources :reviews
  resources :activities

  get '*path', to: 'root#index'
  root 'root#index'
end
